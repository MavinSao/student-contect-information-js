
//Validate

function validateName(name) {
    var patt = /[^a-z]/gi;
    if (name.value == '') {
        document.getElementById('name_msg').innerHTML = "  * Your Name feild can't be empty!";
        document.getElementById('name_msg').style = "color: red";
        return false;
    } else if (name.value.match(patt) == null) {
        document.getElementById('name_msg').innerHTML = "";
        return true;
    } else {
        document.getElementById('name_msg').innerHTML = "  * Your Name is not valid! Only Character, with no space!";
        document.getElementById('name_msg').style = "color: red";
        return false;
    }
}
function validateSchool(school) {
    var patt = /[^a-z\s]/gi;
    if (school.value == '') {
        document.getElementById('school_msg').innerHTML = "  * School name feild can't be empty!";
        document.getElementById('school_msg').style = "color: red";
        return false;
    } else if (school.value.match(patt) == null) {
        document.getElementById('school_msg').innerHTML = "";
        return true;
    } else {
        document.getElementById('school_msg').innerHTML = "  * School Name is not valid! Only Character!";
        document.getElementById('school_msg').style = "color: red";
        return false;
    }
}

function validateContect(contect) {
    var patt = /[^0-9]/gi;
    console.log((contect.value + "").length);

    if (contect.value == '') {
        document.getElementById('contect_msg').innerHTML = "  * Contect feild can't be empty!";
        document.getElementById('contect_msg').style = "color: red";
        return false;
    } else if (contect.value.match(patt) != null){
        document.getElementById('contect_msg').innerHTML = "  * Contect is not valid! Only Number, with no space!";
        document.getElementById('contect_msg').style = "color: red";
        return false;
    } else if ((contect.value + "").length < 9) {
    document.getElementById('contect_msg').innerHTML = "  * Contect has to be at least 9 characters !!";
    document.getElementById('contect_msg').style = "color: red";
    return false;
    } else {
    document.getElementById('contect_msg').innerHTML = "";
    return true;
    }
}
var rindex, table = document.getElementById('table');

//Add to table
document.getElementById("btnAdd").addEventListener("click", Add);
var id = 1;
var rows = 0;
function Add() {

    let name = document.getElementById('studentName');
    let gender = document.getElementById('gender');
    let school = document.getElementById('school');
    let contect = document.getElementById('contect');

    con = validateContect(contect);
    na = validateName(name);
    sc = validateSchool(school);

    if (na == true && sc == true && con == true) {

        var parrent = document.getElementById("parrent");
        let ro = document.createElement('tr');
        let el1 = document.createElement('td');
        let el2 = document.createElement('td');
        let el3 = document.createElement('td');
        let el4 = document.createElement('td');
        let el5 = document.createElement('td');

        let c1 = document.createTextNode(id);
        let c2 = document.createTextNode(name.value);
        let c3 = document.createTextNode(gender.value);
        let c4 = document.createTextNode(school.value);
        let c5 = document.createTextNode(contect.value);


        el1.appendChild(c1);
        el2.appendChild(c2);
        el3.appendChild(c3);
        el4.appendChild(c4);
        el5.appendChild(c5);

        ro.appendChild(el1);
        ro.appendChild(el2);
        ro.appendChild(el3);
        ro.appendChild(el4);
        ro.appendChild(el5);

        parrent.appendChild(ro);

        id++;

        rows = document.getElementById('table').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
        document.getElementById('record').innerHTML = rows;


        for (var i = 1; i < table.rows.length; i++) {
            table.rows[i].onclick = function () {
                // remove the background from the previous selected row
                if (typeof index !== "undefined") {
                    table.rows[rindex].classList.toggle("selected");
                }
                //get the selected row index 
                rindex = this.rowIndex;
                // add class selected to the row
                this.classList.toggle("selected");
            };
        }
    }
}

document.getElementById("btnRemove").addEventListener("click", Remove);

function Remove() {
    if (rows == 0) {
        alert("No record for delete!")
    } else {
        if (isNaN(rindex)) {
            alert("Please Select Record before remove!");
        } else {
            let c = confirm("Do you want to delete ?");
            if (c == true) {
                table.deleteRow(rindex);
            }
        }
        rows = document.getElementById('table').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
        document.getElementById('record').innerHTML = rows;
        rindex = undefined;
    }

}







